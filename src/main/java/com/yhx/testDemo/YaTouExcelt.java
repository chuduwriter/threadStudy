package com.yhx.testDemo;

import com.yhx.security.gfoBank.file.DataConvertUtils;
import net.sourceforge.pinyin4j.PinyinHelper;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * @author: mishuai
 * @date: 2020/11/13 19:58
 * @description:
 */
public class YaTouExcelt {

    public static void main(String[] args) {
        List<Object> ll = ExcelImportUtil.importExcel("/Users/mishuai/work/代码练习/threadStudy/src/main/webapp/cert/顺式元件rename.xlsx", Yatou.class);
        System.out.println(FastJsonUtils.toJSONString(ll));
        System.out.println(ll.size());
        boolean b=false;
        int i=0;
        int j=0;
        List<Yatou> yatous=new ArrayList<>();
       /* if(ll !=null && ll.size()>0){
            for(Object o:ll) {
                Yatou yatou = (Yatou)o;
                if(yatou == null || StringUtils.isBlank(yatou.getH())){
                    j++;
                    continue;
                }
                yatous.add(yatou);
                i++;
            }
        }*/
        System.out.println(i);
        System.out.println(j);

       // ComparatorList comparatorList=new ComparatorList();
       // Collections.sort(shopAddressInfos,comparatorList);

       // ExcelExportUtil.exportToFile("/Users/mishuai/work/代码练习/threadStudy/src/main/webapp/cert/plant2.xlsx",yatous);

    }


    /**
     * 得到中文首字母
     *
     * @param str
     * @return
     */
    public static String getPinYinHeadChar(String str) {
        int tmp= 2;
        if(str.length() < 2){
            tmp = str.length();
        }
        String convert = "";
        for (int j = 0; j < tmp; j++) {
            char word = str.charAt(j);
            String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
            if (pinyinArray != null) {
                convert += pinyinArray[0].charAt(0);
            } else {
                convert += word;
            }
        }
        return convert.toUpperCase();
    }
}
