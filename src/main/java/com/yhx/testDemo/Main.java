package com.yhx.testDemo;

import com.yhx.testDemo.Sm4Utils;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");

        // 密钥
//        byte[] key = {0x01, 0x23, 0x45, 0x67, (byte) 0x89, (byte) 0xab,
//                (byte) 0xcd, (byte) 0xef, (byte) 0xfe, (byte) 0xdc,
//                (byte) 0xba, (byte) 0x98, 0x76, 0x54, 0x32, 0x10};
        String key = "1111111111111111";
        // 明文
        String plainText = "今天星期几啊";

        byte[] enOut = Sm4Utils.sm4EcbEncrypt(key.getBytes(), plainText.getBytes(), "NoPadding");
        if (enOut == null) {
            return;
        }
        System.out.println("加密结果：");
        System.out.println(toHexString(enOut));

        byte[] deOut = Sm4Utils.sm4EcbDecrypt( key.getBytes(), enOut,"NoPadding");
        System.out.println("\n解密结果(return byte[])：");
        System.out.println(new String(deOut));

    }
    public static String toHexString(byte[] byteArray) {
        if (byteArray == null || byteArray.length < 1) {
            throw new IllegalArgumentException("this byteArray must not be null or empty");
        }

        final StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < byteArray.length; i++) {
            if ((byteArray[i] & 0xff) < 0x10) {
                hexString.append("0");
            }
            hexString.append(Integer.toHexString(0xFF & byteArray[i]));
        }
        return hexString.toString().toLowerCase();
    }
}
