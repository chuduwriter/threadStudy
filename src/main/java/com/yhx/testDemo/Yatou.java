package com.yhx.testDemo;

import sun.java2d.pipe.AAShapePipe;

/**
 * @Author: mishuai
 * @Date: 2021/1/31 9:48 下午
 */
@ExcelSheet(name = "sheet2")
public class Yatou {
    @ExcelField(name = "A")
    private String a;
    @ExcelField(name = "B")
    private String b;
    @ExcelField(name = "C")
    private String c;
    @ExcelField(name = "D")
    private String d;
    @ExcelField(name = "E")
    private String e;
    @ExcelField(name = "F")
    private String f;
    @ExcelField(name = "G")
    private String g;
    @ExcelField(name = "H")
    private String h;

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getF() {
        return f;
    }

    public void setF(String f) {
        this.f = f;
    }

    public String getG() {
        return g;
    }

    public void setG(String g) {
        this.g = g;
    }

    public String getH() {
        return h;
    }

    public void setH(String h) {
        this.h = h;
    }
}
