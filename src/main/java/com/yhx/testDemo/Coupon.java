package com.yhx.testDemo;

import java.math.BigDecimal;

/**
 * @Author: mishuai
 * @Date: 2020/6/10 6:02 下午
 */
public class Coupon{
    private BigDecimal prizeAmount;
    private int index;

    public BigDecimal getPrizeAmount() {
        return prizeAmount;
    }

    public void setPrizeAmount(BigDecimal prizeAmount) {
        this.prizeAmount = prizeAmount;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Coupon(BigDecimal prizeAmount, int index) {
        this.prizeAmount = prizeAmount;
        this.index = index;
    }

    @Override
    public String toString() {
        return "Coupon{" +
                "prizeAmount=" + prizeAmount +
                ", index=" + index +
                '}';
    }
}