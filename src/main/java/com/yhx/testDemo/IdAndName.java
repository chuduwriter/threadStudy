package com.yhx.testDemo;

/**
 * @author: mishuai
 * @date: 2021/2/1 15:16
 * @description:
 */
@ExcelSheet(name = "AA")
public class IdAndName {
    @ExcelField(name = "name")
    private String name;
    @ExcelField(name = "id")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
