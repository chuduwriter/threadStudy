package com.yhx.testDemo;

import com.alibaba.fastjson.JSON;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @Author: mishuai
 * @Date: 2020/6/10 6:00 下午
 */
public class bigdecimalTest {

    public static void main(String[] args) {
        BigDecimal bigDecimal=BigDecimal.valueOf(22.01);;
        System.out.println(bigDecimal);
        Coupon coupon1=new Coupon(new BigDecimal(12.1),1);
        Coupon coupon2=new Coupon(new BigDecimal(13.1),2);
        Coupon coupon3=new Coupon(new BigDecimal(11.1),3);
        Coupon coupon4=new Coupon(new BigDecimal(12.1),4);
        Coupon coupon5=new Coupon(new BigDecimal(16.1),5);
        System.out.println(JSON.toJSONString(coupon1));
         List<Coupon> list=new ArrayList<>();
         list.add(coupon1);
        list.add(coupon2);
        list.add(coupon3);
        list.add(coupon4);
        list.add(coupon5);
        sortCouponsByAmount(list);
        System.out.println(JSON.toJSONString(list));
    }


    public static void sortCouponsByAmount(List<? extends Object> couponListForTab){
        if(couponListForTab == null || couponListForTab.size() == 0){
            return ;
        }
        Collections.sort(couponListForTab, new Comparator<Object>() {
            @Override
            public int compare(Object o1, Object o2) {
                if(o1 instanceof Coupon && o2 instanceof Coupon){
                    Coupon coupon1 = (Coupon) o1;
                    Coupon coupon2 = (Coupon) o2;
                    if(coupon1 == null){
                        return -1;
                    }
                    if(coupon2 == null){
                        return  1;
                    }
                    try {
                        if(coupon1.getPrizeAmount().compareTo(coupon2.getPrizeAmount()) >0){
                            return -1;
                        }else if(coupon1.getPrizeAmount().compareTo(coupon2.getPrizeAmount()) == 0){
                            return 0;
                        }
                        return 1;
                    } catch (Exception e){
                        //new Logger(CityCouponService.class).error("优惠券列表排序异常，异常信息为：",e);
                    }
                }
                return 0;
            }
        });


    }


}
