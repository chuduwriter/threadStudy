package com.yhx.testDemo;

import java.util.*;

/**
 * @author: mishuai
 * @date: 2020/11/26 21:18
 * @description:
 */
public class SetException {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        Integer [] a={50, 42, 56, 76, 62, 38, 42, 98, 66, 91, 53, 56, 97, 61, 33, 26, 97, 45};
        list=Arrays.asList(a);
        //遍历list去除奇数
        Iterator<Integer> iterator = list.iterator();//获取ArrayList的迭代器
        Set<Integer> s=new HashSet<>();

        //迭代器遍历
        try{
            while(iterator.hasNext()){
                Integer tempNum = iterator.next();
                if (tempNum % 2 != 0){
                    list.remove(tempNum);//报错ConcurrentModificationException（并发修改异常）
                }
            }
        }catch (Exception e){
            System.out.println(e);
        }

    }
}
