package com.yhx.studyDemo.department;

/**
 * @author mishuai
 * @version 1.0
 * @description:
 * @date 2021/4/22 11:32
 */


import com.yhx.testDemo.FastJsonUtils;
import org.apache.commons.lang.StringUtils;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 多路搜索树数据结构
 * <p>此处以最基础的SSM框架为案例</p>
 */

public class TreeDataService {

    public static void main(String[] args) {
        TreeDataService treeDataService=new TreeDataService();
        MultiplexingTreeNode multiplexingTreeNode=treeDataService.getTreeDataByTreeNo("");
        System.out.println(FastJsonUtils.toJSONString(multiplexingTreeNode));
    }

    /**
     * 根据树标识获取整个业务树
     *
     * @param treeNo 树标识
     * @return 业务对应多路搜索树
     */
    public MultiplexingTreeNode getTreeDataByTreeNo(String treeNo) {
 /*       TreeDataExample example = new TreeDataExample();
        example.createCriteria().andTreeNoEqualTo(treeNo);
        example.setOrderByClause(" idx ");
        LinkedList<TreeData> treeDataList = treeDataMapper.selectByExample(example);*/

        LinkedList<TreeData> treeDataList =new LinkedList<>();
        TreeData t=new TreeData();
        t.setTreeCode("0");
        t.setTreeDesc("0");
        TreeData t1=new TreeData();
        t1.setTreeCode("1");
        t1.setTreeDesc("1");
        t1.setParentTreeCode("0");
        TreeData t2=new TreeData();
        t2.setTreeCode("2");
        t2.setTreeDesc("2");
        t2.setParentTreeCode("1");
        TreeData t3=new TreeData();
        t3.setTreeCode("3");
        t3.setTreeDesc("3");
        t3.setParentTreeCode("1");
        TreeData t4=new TreeData();
        t4.setTreeCode("4");
        t4.setTreeDesc("4");
        t3.setParentTreeCode("2");
        treeDataList.add(t);
        treeDataList.add(t1);
        treeDataList.add(t2);
        treeDataList.add(t3);
        treeDataList.add(t4);
        MultiplexingTreeNode multiplexingTreeNode= createMultiplexingTree(treeDataList);
        return multiplexingTreeNode;
    }

    private MultiplexingTreeNode createMultiplexingTree(LinkedList<TreeData> treeDataList) {
        MultiplexingTreeNode node = null;
        if (treeDataList == null || treeDataList.isEmpty()) {
            return null;
        }
        TreeData treeData = treeDataList.removeFirst();

        if (!StringUtils.isEmpty(treeData.getTreeCode())) {
            node = new MultiplexingTreeNode(treeData);


            while (true) {

                MultiplexingTreeNode binaryTree = createMultiplexingTree(treeDataList);
                if (binaryTree == null) {
                    break;
                }
                node.addChildNodeList(binaryTree);
            }
        }
        return node;
    }

    public static class MultiplexingTreeNode {

        private String key;

        private String name;

        private String parentKey;

        private LinkedList<MultiplexingTreeNode> childNodeList;

        public MultiplexingTreeNode() {}

        public MultiplexingTreeNode(TreeData treeData) {
            this.key = treeData.getTreeCode();
            this.name = treeData.getTreeDesc();
            this.parentKey = treeData.getParentTreeCode();
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getParentKey() {
            return parentKey;
        }

        public void setParentKey(String parentKey) {
            this.parentKey = parentKey;
        }

        public void setChildNodeList(LinkedList<MultiplexingTreeNode> childNodeList) {
            this.childNodeList = childNodeList;
        }

        public LinkedList<MultiplexingTreeNode> getChildNodeList() {
            return childNodeList;
        }

        public void addChildNodeList(MultiplexingTreeNode childNode) {
            if (childNodeList == null) {
                childNodeList = new LinkedList<>();
            }
            childNodeList.add(childNode);
        }
    }

    /**
     * 根据节点码获取该节点及其子节点。
     *
     * <p>获取方式：广度优先原则</p>
     * <p>根据节点码获取该节点对应的子树</p>
     *
     * @param treeNo 树标识
     * @param treeCode 节点标识
     * @return treeCode节点对应树
     */
    public MultiplexingTreeNode getNodeByTreeCode(String treeNo, String treeCode) {
        MultiplexingTreeNode rootTreeNode = getTreeDataByTreeNo(treeNo);
        Queue<MultiplexingTreeNode> queue = new LinkedList<>();
        queue.offer(rootTreeNode);

        while (!queue.isEmpty()) {
            MultiplexingTreeNode currTreeNode = queue.poll();
            if (treeCode.equals(currTreeNode.getKey())) {
                return currTreeNode;
            }
            if(treeCode.equals(currTreeNode.getParentKey())){

            }
            LinkedList<MultiplexingTreeNode> childNodeList = currTreeNode.getChildNodeList();
            if (childNodeList != null) {
                for (MultiplexingTreeNode childNode : childNodeList) {
                    queue.offer(childNode);
                }
            }
        }
        return null;
    }


}
