package com.yhx.studyDemo.department;

/**
 * @author mishuai
 * @version 1.0
 * @description:
 * @date 2021/4/22 11:34
 */
public class TreeData {

    private String treeCode;

    private String treeDesc;

    private String parentTreeCode;

    public String getTreeCode() {
        return treeCode;
    }

    public void setTreeCode(String treeCode) {
        this.treeCode = treeCode;
    }

    public String getTreeDesc() {
        return treeDesc;
    }

    public void setTreeDesc(String treeDesc) {
        this.treeDesc = treeDesc;
    }

    public String getParentTreeCode() {
        return parentTreeCode;
    }

    public void setParentTreeCode(String parentTreeCode) {
        this.parentTreeCode = parentTreeCode;
    }

    @Override
    public String toString() {
        return "TreeData{" +
                "treeCode='" + treeCode + '\'' +
                ", treeDesc='" + treeDesc + '\'' +
                ", parentTreeCode='" + parentTreeCode + '\'' +
                '}';
    }
}
