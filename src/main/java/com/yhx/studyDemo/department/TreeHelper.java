package com.yhx.studyDemo.department;


import com.yhx.testDemo.FastJsonUtils;

import java.util.*;

public class TreeHelper {

    private TreeNode root;
    //private List<TreeNode> tempNodeList;


    public TreeHelper() {
    }

    public static void main(String[] args) {
        List<TreeNode> tempNodes=new ArrayList<>();
        TreeHelper treeHelper=new TreeHelper();

        List<OrganizationEntity> list=new ArrayList<>();
        OrganizationEntity o=new OrganizationEntity();
        o.setOrgId(1);
        o.setOrgName("1");
        o.setParentId(0);
        list.add(o);

        OrganizationEntity o1=new OrganizationEntity();
        o1.setOrgId(7);
        o1.setOrgName("7");
        o1.setParentId(0);
        list.add(o1);

        OrganizationEntity o2=new OrganizationEntity();
        o2.setOrgId(3);
        o2.setOrgName("3");
        o2.setParentId(1);
        list.add(o2);


        OrganizationEntity o3=new OrganizationEntity();
        o3.setOrgId(4);
        o3.setOrgName("4");
        o3.setParentId(2);
        list.add(o3);


        OrganizationEntity o4=new OrganizationEntity();
        o4.setOrgId(5);
        o4.setOrgName("5");
        o4.setParentId(1);
        list.add(o4);

        OrganizationEntity o5=new OrganizationEntity();
        o5.setOrgId(6);
        o5.setOrgName("6");
        o5.setParentId(4);
        list.add(o5);
        List<TreeNode> tempNodeLists=treeHelper.changeEnititiesToTreeNodes(list);
        HashMap<String,TreeNode> hashMap=treeHelper.generateTree(tempNodeLists);
        //TreeNode treeNode=hashMap.get("1");
        //System.out.println(FastJsonUtils.toJSONString(treeNodes));
        List<TreeNode> treeNodeList=treeHelper.generateListTree(tempNodeLists,0l);
        //System.out.println(FastJsonUtils.toJSONString(treeNodeList));
        //System.out.println(FastJsonUtils.toJSONString(hashMap.get("1")));
        //HashMap hashMap=treeHelper.putNodesIntoMap();
        //System.out.println(FastJsonUtils.toJSONString(hashMap));
    }

    public HashMap generateTree(List<TreeNode> tempNodeList) {
        HashMap nodeMap = putNodesIntoMap(tempNodeList);
        //System.out.println(FastJsonUtils.toJSONString(nodeMap));
        putChildIntoParent(nodeMap);
        return nodeMap;
    }

    public List<TreeNode> generateListTree(List<TreeNode> tempNodeList,Long pid) {
        //System.out.println(FastJsonUtils.toJSONString(nodeMap));
        List<TreeNode> list=getTreeNodeList(tempNodeList,pid);
        System.out.println(FastJsonUtils.toJSONString(list));
        return list;
    }

    protected HashMap<String, TreeNode> putNodesIntoMap(List<TreeNode> tempNodeList) {
        long maxId = Long.MAX_VALUE;
        HashMap<String, TreeNode> nodeMap = new HashMap<String, TreeNode>();
        for(TreeNode treeNode:tempNodeList){
            long id = treeNode.getId();
            if (id < maxId) {
                maxId = id;
                this.root = treeNode;
            }
            String keyId = String.valueOf(id);
            nodeMap.put(keyId, treeNode);
        }
        return nodeMap;
    }


    protected void putChildIntoParent(HashMap<String,TreeNode> nodeMap) {
        Iterator it = nodeMap.values().iterator();

        while (it.hasNext()) {
            TreeNode treeNode = (TreeNode) it.next();
            long parentId = treeNode.getParentId();
            String parentKeyId = String.valueOf(parentId);
            if (nodeMap.containsKey(parentKeyId)) {
                TreeNode parentNode = nodeMap.get(parentKeyId);
                if (parentNode == null) {
                    return;
                } else {
                    parentNode.addChildNode(treeNode);
                }
            }
        }
    }

    protected void putChildIntoParentById(HashMap<String,TreeNode> nodeMap) {
        Iterator it = nodeMap.values().iterator();
        String currentId="2";
        List<String> stringList=new ArrayList<>();
        stringList.add(currentId);

        while (it.hasNext()) {
            TreeNode treeNode = (TreeNode) it.next();
            long parentId = treeNode.getParentId();
            String parentKeyId = String.valueOf(parentId);
            if (nodeMap.containsKey(parentKeyId)) {
                TreeNode parentNode = nodeMap.get(parentKeyId);
                if (parentNode == null) {
                    return;
                } else {
                    parentNode.addChildNode(treeNode);
                }
            }
        }
    }


    public  List<TreeNode> getTreeNodeList(List<TreeNode> treeNodeList, long pid){
        //System.out.println(FastJsonUtils.toJSONString(treeNodeList));
        List<TreeNode> childNodes=new ArrayList<TreeNode>();
        for(TreeNode treeNode: treeNodeList){
            //遍历出父id等于参数的id，add进子节点集合
            if(treeNode.getParentId() == pid){
                //递归遍历下一级
                getTreeNodeList(treeNodeList,treeNode.getId());
                childNodes.add(treeNode);
            }
        }
        return childNodes;

    }

    public static List<TreeNode> changeEnititiesToTreeNodes(List<OrganizationEntity> entityList) {
        OrganizationEntity orgEntity = new OrganizationEntity();
        List<TreeNode> tempNodeList = new ArrayList<TreeNode>();
        TreeNode treeNode;

        Iterator it = entityList.iterator();
        while (it.hasNext()) {
            orgEntity = (OrganizationEntity) it.next();
            treeNode = new TreeNode();
            treeNode.setParentId(orgEntity.getParentId());
            treeNode.setId(orgEntity.getOrgId());
            treeNode.setDepartMentName(orgEntity.getOrgName());
            tempNodeList.add(treeNode);
        }
        return tempNodeList;
    }




}