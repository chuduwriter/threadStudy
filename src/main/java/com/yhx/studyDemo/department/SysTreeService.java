package com.yhx.studyDemo.department;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.yhx.testDemo.FastJsonUtils;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * @author mishuai
 * @version 1.0
 * @description:
 * @date 2021/4/22 13:43
 */
public class SysTreeService {


    public static void main(String[] args) {
        SysTreeService sysTreeService=new SysTreeService();
        System.out.println(FastJsonUtils.toJSONString(sysTreeService.deptTree()));
    }
    public List<DeptLevelDto> deptTree(){
        //1.查询所有部门
        List<SysDept> deptList = new ArrayList<>();
        SysDept t=new SysDept();
        t.setId(0);
        t.setTreeDesc("0");
        t.setLevel("0");
        SysDept t1=new SysDept();
        t1.setId(1);
        t1.setTreeDesc("1");
        t1.setParentId(0);
        t1.setLevel("0.1");
        SysDept t2=new SysDept();
        t2.setId(2);
        t2.setTreeDesc("2");
        t2.setParentId(0);
        t2.setLevel("0.2");
        SysDept t3=new SysDept();
        t3.setId(3);
        t3.setTreeDesc("3");
        t3.setParentId(1);
        t3.setLevel("0.2.1");
        SysDept t4=new SysDept();
        t4.setId(4);
        t4.setTreeDesc("4");
        t4.setParentId(0);
        t4.setLevel("0.4");
       // deptList.add(t);
        deptList.add(t1);
        deptList.add(t2);
        deptList.add(t3);
        deptList.add(t4);
        //2.创建dto集合
        List<DeptLevelDto> dtoList = new ArrayList();
        //3.将所有部门集合转化为Dto集合
        for (SysDept dept : deptList) {
            dtoList.add(DeptLevelDto.adapt(dept));
        }
        //4.创建方法组装tree
        return deptDtoList2Tree(dtoList);
    }

    private List<DeptLevelDto> deptDtoList2Tree(List<DeptLevelDto> deptDtoList){
        //1.判断非空
        if (CollectionUtils.isEmpty(deptDtoList)){
            return Lists.newArrayList();
        }
        Multimap<String, DeptLevelDto> levelDeptMap = ArrayListMultimap.create();
        List<DeptLevelDto> rootList = new ArrayList();
        for (DeptLevelDto dto : deptDtoList) {
            //2.将所有节点拿出来存到一个Multimap中,key是level
            levelDeptMap.put(dto.getLevel(), dto);
            //3.将根节点拿出来存到一个新的List<DeptLevelDto>中
            if (dto.getLevel().equals(LevelUtils.ROOT)){
                rootList.add(dto);
            }
        }
        //4.按照seq排序rootList
     /*   Collections.sort(rootList, new Comparator<DeptLevelDto>() {
            @Override
            public int compare(DeptLevelDto o1, DeptLevelDto o2) {
                return o1.getSeq()-o2.getSeq();
            }
        });*/
        //5.递归生成树
        transFormDeptTree(rootList,LevelUtils.ROOT,levelDeptMap);
        return rootList;
    }

    private void transFormDeptTree(List<DeptLevelDto> deptLevelList,String level,Multimap<String, DeptLevelDto> levelDeptMap){
        //1.遍历当前层级数据
        for (DeptLevelDto dto : deptLevelList) {
            //2.获取下一层的lever
            String nextLevel = LevelUtils.getLevel(dto.getId(), level);
            //3.通过下一层的lever从map里拿数据
            List<DeptLevelDto> nextDeptList = (List<DeptLevelDto>) levelDeptMap.get(nextLevel);
            //4.如果nextDeptList不为空就进行处理
            if (!CollectionUtils.isEmpty(nextDeptList)){
                //4.将nextDeptList排序
           /*     Collections.sort(nextDeptList, new Comparator<DeptLevelDto>() {
                    @Override
                    public int compare(DeptLevelDto o1, DeptLevelDto o2) {
                        return o1.getSeq()-o2.getSeq();
                    }
                });*/
                //5.组装到rootTree
                dto.setDeptList(nextDeptList);
                //6.进行下一层迭代
                transFormDeptTree(nextDeptList,nextLevel,levelDeptMap);
            }else{
                return;
            }

        }
    }
}
