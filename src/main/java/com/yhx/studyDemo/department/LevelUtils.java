package com.yhx.studyDemo.department;

import org.apache.commons.lang3.StringUtils;

/**
 * @author mishuai
 * @version 1.0
 * @description:
 * @date 2021/4/22 13:44
 */
public class LevelUtils {
    public final static String ROOT = "0";

    public final static String SEPARATOR = ".";


    public static String getLevel(Integer parentId , String parentLevel){
        //如果parentId==0或为空,这返回ROOT
        if (parentId==null || parentId==0 ){
            return ROOT;
        }
        //否则返回parentLevel.parentId
        return StringUtils.join(parentLevel,SEPARATOR,parentId);
    }
}
