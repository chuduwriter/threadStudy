package com.yhx.studyDemo.lambda;

public class Parameter {

    public static void main(String[] args) {
        display(new Test() {//把匿名内部类作为参数传递，可以直接重写抽象类中的方法；
            @Override
            public void show() {
                System.out.println("重写过的show方法：");
            }
        });

        System.out.println("-----------------------------------");

        Test2 test2= new Test2(){ //可以利用多态，将匿名对象指向test2；
            @Override
            public void show1() {
                System.out.println("重写过的show1方法：");
            }
        };
        display2(test2); //通过参数传递，也可以达到调用目的；


    }

    public static  void  display(Test test) {//设置为静态方法，进入主函数后就可以直接调用了；
        test.show();
    }

    public  static  void display2(Test2 test2) {
        test2.show1();
    }
}
