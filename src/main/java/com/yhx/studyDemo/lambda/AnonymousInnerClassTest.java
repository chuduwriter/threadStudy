package com.yhx.studyDemo.lambda;

import com.yhx.testDemo.FastJsonUtils;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AnonymousInnerClassTest {

    private  void testAnonymousInnerClass(FunctionalInterfaceDemo functionalInterfaceDemo){
        Integer number=1;
        boolean b=functionalInterfaceDemo.test(number);
        System.out.println(b);
    }

    public static void main(String[] args) {
        AnonymousInnerClassTest anonymousInnerClassTest=new AnonymousInnerClassTest();

        anonymousInnerClassTest.testAnonymousInnerClass(new FunctionalInterfaceDemo() {
            @Override
            public boolean test(Integer x) {
                return true;
            }
        });

        anonymousInnerClassTest.testAnonymousInnerClass( number ->{
            return true;
        });

        List<Student> students=new ArrayList<>();
        Student s=new Student();
        s.setStudentNumber(0l);
        Student s1=new Student();
        s1.setStudentNumber(1l);
        Student s2=new Student();
        s2.setStudentNumber(2l);
        Student s3=new Student();
        s3.setStudentNumber(3l);
        Student s4=new Student();
        s4.setStudentNumber(4l);
        Student s5=new Student();
        s5.setStudentNumber(5l);
        Student s6=new Student();
        s6.setStudentNumber(6l);
        Student s7=new Student();
        s7.setStudentNumber(7l);
        students.add(s);
        students.add(s5);
        students.add(s1);
        students.add(s2);
        students.add(s3);
        students.add(s4);
        students.add(s6);
        students.add(s7);
        Map<Long, List<Student>> map=anonymousInnerClassTest.useFor(students);
        System.out.println(FastJsonUtils.toJSONString(map));
        Map<Long, List<Student>> map1 =students.stream().collect(Collectors.groupingBy(Student::getStudentNumber));
        System.out.println(FastJsonUtils.toJSONString(map1));

        List<Student> list=students.stream().filter(ss->ss.getStudentNumber()>3).collect(Collectors.toList());
        System.out.println(FastJsonUtils.toJSONString(list));

        List<Long> longList=students.stream().map(Student::getStudentNumber).collect(Collectors.toList());
        System.out.println(FastJsonUtils.toJSONString(longList));

        List<Student> sortList=students.stream().sorted(Comparator.comparing(Student::getStudentNumber).reversed()).collect(Collectors.toList());
        System.out.println(FastJsonUtils.toJSONString(sortList));

        Optional<Student> student=students.stream().findAny();
        Student st=student.get();
        System.out.println(FastJsonUtils.toJSONString(st));

        Long l=students.stream().count();
        System.out.println(l);

        Long totalL=longList.stream().reduce((total,ll)->total+ll).get();

        Long streamTotal=longList.stream().reduce(new BinaryOperator<Long>() {
            @Override
            public Long apply(Long aLong, Long aLong2) {
                return aLong+aLong2;
            }
        }).get();
        System.out.println(totalL);

        Long streamTotal1=longList.stream().reduce(/*new BinaryOperator<Long>() {
            @Override
            public Long apply(Long aLong, Long aLong2) {
                return aLong+aLong2;
            }
        }*/
          (along, aLong2) -> along+aLong2
        ).get();

        Long longlong=longList.stream().reduce(1l,(aLong, aLong2) -> (aLong+aLong2));
        System.out.println(longlong);


    }

    private void createByCollection() {
        List<Integer> list = new ArrayList<>();
        Stream<Integer> stream = list.stream();
    }

    /**
     * ᭗ᬦහᕟ຅᭜ၞ
     */
    private void createByArrays() {
        Integer[] intArrays = {1, 2, 3};
        Stream<Integer> stream = Stream.of(intArrays);
        Stream<Integer> stream1 = Arrays.stream(intArrays);
    }

    public Map<Long, List<Student>> useFor(List<Student> students) {
        Map<Long, List<Student>> map = new HashMap<>();
        for (Student student : students) {
            List<Student> list = map.get(student.getStudentNumber());
            if (list == null) {
                list = new ArrayList<>();
                map.put(student.getStudentNumber(), list);
            }
            list.add(student);
        }
        return map;
    }


}
