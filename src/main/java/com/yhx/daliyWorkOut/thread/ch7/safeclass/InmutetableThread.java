package com.yhx.daliyWorkOut.thread.ch7.safeclass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mishuai
 * Date: 2018-11-16
 * Time: 09:09
 * Des:
 */
public class InmutetableThread {

    private   List<User> list =  new ArrayList<User>(3);

     User user=new User("zhangsan",10);
    public InmutetableThread() {

        System.out.println(list.add(user));
        list.add(new User("lisi",12));
        list.add(new User("wanger",13));
    }


    public  boolean isContains() {
        return list.contains(user);
    }

    public void contains(){
        System.out.println(isContains());
    }

    public static void main(String[] args) {

        InmutetableThread inmutetableThread=new InmutetableThread();


    }

    private static class User{

        private String name;

        private Integer age;

        public User(){}
        public User(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }
    }

}
