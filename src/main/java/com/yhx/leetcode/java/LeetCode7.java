package com.yhx.leetcode.java;

/**
 * @Author: mishuai
 * @Date: 2020/7/1 2:19 下午
 */
public class LeetCode7 {


    public static void main(String[] args) {
        System.out.println(reverse(964632435));
    }


    public static int reverse(int x) {
        int flag=1;
        if(x <0){
            x=-x;
            flag=0;
        }
        String xstr= String.valueOf(x);
        char c[]=xstr.toCharArray();
        int zeroFlag=0;
        StringBuilder sb=new StringBuilder();
        for(int i=c.length-1;i>=0;i--){
            if(c[i] != '0' && zeroFlag == 0 ){
                zeroFlag=1;
            }
            if(zeroFlag == 0){
                continue;
            }
            String re=Character.toString(c[i]);
            sb.append(re);
        }

        Integer in=Integer.parseInt(sb.toString());
        return flag == 0? -in:in;

    }
}
