package com.yhx.leetcode.java;

/**
 * @Author: mishuai
 * @Date: 2020/7/2 4:11 下午
 */
public class LeetCode11 {


    public static void main(String[] args) {
        int[] in={1,8,6,2,5,4,8,3,7};
        System.out.println(maxArea(in));
    }

    //双指针解法
    public static int maxArea(int[] height) {

        if(height.length <2) return 0;
        int leftPointer=1,rightPointer=height.length - 1;
        int resultArea=0;
        while( leftPointer < rightPointer){
            int area=Math.min(height[rightPointer],height[leftPointer]) * (rightPointer - leftPointer);
            resultArea=Math.max(resultArea,area);
            if(height[leftPointer] <= height[rightPointer]){
                ++ leftPointer ;
            }else{
                -- rightPointer;
            }
        }
        return resultArea;
    }
}


