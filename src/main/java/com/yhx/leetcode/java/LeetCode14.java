package com.yhx.leetcode.java;

/**
 * @Author: mishuai
 * @Date: 2020/7/6 3:22 下午
 */
public class LeetCode14 {

    public static void main(String[] args) {
        String str[]={"z1hangsan32","zhangsan2","zhangsan3"};
        System.out.println(longestCommomPrefix(str));
    }


    public static String longestCommomPrefix(String str[]){

        String ans=str[0];
        for(int i=1;i<str.length;i++){
            if(str[i] == null || str[i].equals("")) return "";
            int j=0;
            for( ;j<ans.length() && j<str[i].length();j++){
                if(  ans.charAt(j) != str[i].charAt(j)){
                    break;
                }
            }
            ans=ans.substring(0,j);
        }
        return ans;
    }
}
