package com.yhx.share.thisShare;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.security.Key;
import java.security.SecureRandom;

/**
 * DES(Data Encryption Standard)是一种对称加密算法，所谓对称加密就是加密和解密都是使用同一个密钥。
 *
 * 加密原理：
 * Rijndael算法首先是一个密钥分组加密的算法，通过置换（permutations ）和替换(substitutions）迭代加密，进过多轮操作形成密文。AES算是Rijndael算法的一种特殊实现，选的分组为128bit（16字节），密钥可以使用128、192 和 256bit三种。
 *
 * DES 使用一个 56 位的密钥以及附加的 8 位奇偶校验位，产生最大 64 位的分组大小。这是一个迭代的分组密码，使用称为 Feistel 的技术，其中将加密的文本块分成两半。使用子密钥对其中一半应用循环功能，然后将输出与另一半进行"异或"运算;接着交换这两半，这一过程会继续下去，但最后一个循环不交换。DES 使用 16 个循环，使用异或，置换，代换，移位操作四种基本运算。
 *
 * 不过，DES已可破解，所以针对保密级别特别高的数据推荐使用非对称加密算法。
 */
public class DESUtil {

    public DESUtil() {
    }
    //测试
    public static void main(String args[]) {
        //待加密内容
        String str = "123456";
        String password = "1234567812345678";

        byte[] result = encrypt(str.getBytes(),password);
        System.out.println("encrypt:"+new String(result));
        //直接将如上内容解密
        try {
            byte[] decryResult = decrypt(result, password);
            System.out.println("decrypt:"+new String(decryResult));
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
    /**
     * 加密
     * @param datasource byte[]
     * @param password String
     * @return byte[]
     */
    public static  byte[] encrypt(byte[] datasource, String password) {
        try{
            SecureRandom random = new SecureRandom();
            DESKeySpec desKey = new DESKeySpec(password.getBytes());
            //创建一个密匙工厂，然后用它把DESKeySpec转换成密钥
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey securekey = keyFactory.generateSecret(desKey);
            //Cipher对象实际完成加密操作
            Cipher cipher = Cipher.getInstance("DES");
            //用密匙初始化Cipher对象
            cipher.init(Cipher.ENCRYPT_MODE, securekey, random);
            //现在，获取数据并加密
            //正式执行加密操作
            return cipher.doFinal(datasource);
        }catch(Throwable e){
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 解密
     * @param src byte[]
     * @param password String
     * @return byte[]
     * @throws Exception
     */
    public static byte[] decrypt(byte[] src, String password) throws Exception {
        // DES算法要求有一个可信任的随机数源
        SecureRandom random = new SecureRandom();
        // 创建一个DESKeySpec对象
        DESKeySpec desKey = new DESKeySpec(password.getBytes());
        // 创建一个密匙工厂
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        // 将DESKeySpec对象转换成SecretKey对象
        SecretKey securekey = keyFactory.generateSecret(desKey);
        // Cipher对象实际完成解密操作
        Cipher cipher = Cipher.getInstance("DES");
        // 用密匙初始化Cipher对象
        cipher.init(Cipher.DECRYPT_MODE, securekey, random);
        // 真正开始解密操作
        return cipher.doFinal(src);
    }
}
