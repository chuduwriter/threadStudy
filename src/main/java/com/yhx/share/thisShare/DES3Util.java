package com.yhx.share.thisShare;




import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
/**
 * https://blog.csdn.net/yang3wei/article/details/7607103
 * 3DES加密解密方式
 * 3DES使用“密钥包”，其包含3个DES密钥，K1，K2和K3，均为56位（除去奇偶校验位）。加密算法为：
 *
 * 　　密文 = EK3（DK2（EK1（平文）））也就是说，使用K1为密钥进行DES加密，再用K2为密钥进行DES“解密”，最后以K3进行DES加密。
 *
 * 　　而解密则为其反过程：
 *
 * 　　平文 = DK1（EK2（DK3（密文）））即以K3解密，以K2“加密”，最后以K1解密。
 *
 * 　　每次加密操作都只处理64位数据，称为一块。
 *
 * 　　无论是加密还是解密，中间一步都是前后两步的逆。这种做法提高了使用密钥选项2时的算法强度，并在使用密钥选项3时与DES兼容。
 * @author Administrator
 *
 */
public class DES3Util {

    private static final String KEY_ALGORITHM = "DESede";
    private static final String DEFAULT_CIPHER_ALGORITHM = "DESede/ECB/PKCS5Padding";// 默认的加密算法

    /**
     * DESede 加密操作
     *
     * @param content
     *            待加密内容
     * @param key
     *            加密密钥
     * @return 返回Base64转码后的加密数据
     */
    public static String encrypt(String content, String key) {
        try {
            Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
            // 创建密码器
            byte[] byteContent = content.getBytes("utf-8");
            cipher.init(Cipher.ENCRYPT_MODE, getSecretKey(key));
            // 初始化为加密模式的密码器
            byte[] result = cipher.doFinal(byteContent);// 加密
            return Base64.encodeBase64String(result);// 通过Base64转码返回
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * } } DESede 解密操作
     *
     * @param content
     * @param key
     * @return
     */
    public static String decrypt(String content, String key) {
        try {
            // 实例化
            Cipher cipher = Cipher.getInstance(KEY_ALGORITHM); // 使用密钥初始化，设置为解密模式
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey(key)); // 执行操作
            byte[] result = cipher.doFinal(Base64.decodeBase64(content));
            return new String(result, "utf-8");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     }
     * 生成加密秘钥
     *
     * @return
     */ private static SecretKeySpec getSecretKey(final String key) {
        //返回生成指定算法密钥生成器的KeyGenerator 对象
        KeyGenerator kg = null;
        try {
            //此类提供秘密（对称）密钥生成器的功能。
            kg = KeyGenerator.getInstance(KEY_ALGORITHM);

            /* 与算法无关的初始化 所有密钥生成器都共享密钥大小和随机源的概念。 此KeyGenerator类中有一个init方法，它采用这两种通用共享类型的参数。 还有一个只需要keysize参数，并使用优先级最高的安装提供程序的SecureRandom实现作为随机源（如果没有安装的提供程序提供SecureRandom实现，则使用系统提供的随机源），以及只需要随机源的一个。
            因为当你调用上述与算法无关未指定其他参数init方法，它是由供应商做一下具体的算法的参数（如果有的话）要与每个键的相关内容。
            特定于算法的初始化
            */
            kg.init(new SecureRandom(key.getBytes()));
            //生成一个密钥
            SecretKey secretKey = kg.generateKey();
            return new SecretKeySpec(secretKey.getEncoded(), KEY_ALGORITHM);
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } return null;
    }

    public static void main(String[] args) {
        String content = "kuangtuzhangsan";
        String key = "123456789123456789";
        System.out.println("content:" + content);
        String s1 = DES3Util.encrypt(content, key);
        System.out.println("encrypt:" + s1);
        System.out.println("decrypt:" + DES3Util.decrypt(s1, key));
    }



}


