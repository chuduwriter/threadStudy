package com.yhx.security.boc;



import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

import com.alibaba.fastjson.JSON;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;


public class SignatureUtil {
    private final static String SIGN_TYPE_RSA = "RSA";
    private final static String SIGN_ALGORITHMS = "SHA1WithRSA";
    private final static String CHARSETTING = "UTF-8";

    /**
     * 获取私钥PKCS8格式（需base64）
     * @param algorithm
     * @param priKey
     * @return PrivateKey
     * @throws Exception
     */
    public static PrivateKey getPrivateKeyFromPKCS8(String algorithm, String priKey) throws Exception {
        if (algorithm == null || "".equals(algorithm) || priKey == null || "".equals(priKey))
            return null;

        KeyFactory keyFactory = KeyFactory.getInstance(algorithm);

        byte[] encodedKey = StreamUtil.readText(new ByteArrayInputStream(priKey.getBytes())).getBytes();
        encodedKey = Base64.decodeBase64(priKey.getBytes());

        return keyFactory.generatePrivate(new PKCS8EncodedKeySpec(encodedKey));
    }

    /**
     * 通过证书获取公钥（需BASE64，X509为通用证书标准）
     * @param algorithm
     * @param pubKey
     * @return PublicKey
     * @throws Exception
     */
    public static PublicKey getPublicKeyFromX509(String algorithm, String pubKey) throws Exception {

        if (algorithm == null || "".equals(algorithm) || pubKey == null || "".equals(pubKey))
            return null;

        KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
        StringWriter writer = new StringWriter();
        StreamUtil.io(new InputStreamReader(new ByteArrayInputStream(pubKey.getBytes())), writer);

        byte[] encodeByte = writer.toString().getBytes();
        encodeByte = Base64.decodeBase64(pubKey.getBytes());

        return keyFactory.generatePublic(new X509EncodedKeySpec(encodeByte));
    }

    /**
     * 使用私钥对字符进行签名
     * @param plain  内容体
     * @param prikey  私钥
     * @return String
     * @throws Exception
     */
    public static String sign(String plain, String prikey) throws Exception {
        if (plain == null || "".equals(plain) || prikey == null || "".equals(prikey))
            return null;

        PrivateKey privatekey = getPrivateKeyFromPKCS8(SIGN_TYPE_RSA, prikey);
        Signature signature = Signature.getInstance(SIGN_ALGORITHMS);
        signature.initSign(privatekey);
        signature.update(plain.getBytes(CHARSETTING));
        byte[] signed = signature.sign();

        return new String(Base64.encodeBase64(signed));
    }

    /**
     * 将内容体、签名信息、及对方公钥进行验签
     * @param plain  内容体
     * @param sign   签名信息
     * @param pubkey  对方公钥
     * @return boolean
     * @throws Exception
     */
    public static boolean virefy(String plain, String sign, String pubkey) throws Exception {
        if (plain == null || "".equals(plain) || sign == null || "".equals(sign) || pubkey == null || "".equals(pubkey))
            return false;

        PublicKey publicKey = getPublicKeyFromX509(SIGN_TYPE_RSA, pubkey);
        Signature signature = Signature.getInstance(SIGN_ALGORITHMS);
        signature.initVerify(publicKey);
        signature.update(plain.getBytes(CHARSETTING));

        return signature.verify(Base64.decodeBase64(sign.getBytes()));
    }

    /**
     * 测试
     * @param args
     * @throws Exception
     */
    //@Test
    public static void main(String[] args) throws Exception {
        //**  私钥密钥此处简单演示，应做成可配置    **/
        String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAMwJf6SDuNirQKl6/pgC3WQDV10mP2Mwwivqzcb0BH4jE8wIeaOGQ5eBw8ncuz1xAFLA6RE0U7I/RxBehIGCKSMYFYfv1Mzd0y3UIXqkJUCyNgrJTu3vlEvydG3PVOeVn0jb6F55TJj1cO/Es14nwVhskjPk/Ft7ftrqoEBmgoidAgMBAAECgYAg1taIb/rsRIPfwz/+z1c6pZ8GCwXgvRRDZUNBZjzi4FprWGHbg9yVIfmVH8WzGeDncM0SS828vpp9c/j3ry9XgRDh70e2LKovEy9rXNenLyNjdQGCaH9WEcNaMrAwW/p2+a1DOZjtRc01yuLW/jNIlI4Sy8LmZ5bRqcp3bcjDsQJBAOgkUqBc+/rFoSxPs9+HoOU0NDpuKAQyUgTJEvlyYpzQ/ZixOy9gOygm0iCAddKgnzJMi4W9o0YlT5o3lX7aKUsCQQDhAbxxHWYv1aM98RJKOMHnvSO6Jvnn2HLdCL8qrMKdGADMexJsrJy9mXCkeYlEFWUQLAtZYQiHvbNL18trroy3AkBZC83SC7jweayYZb5WqRzzrrG2FBkveunxQfwQSWtAQf50+s78Hkqy3SlPJFeNwuUuEySV2aduudMuEdI7hY2/AkArE80DDvDYaZtWKYgp45HkDwb/BaVEqODcxmbrAaZEsyq7+zf8zFM5zV2Ob6JDAaGWpggKNZSPgFcKRycv13wjAkEAxO1AqU/hwfyZ8hSaROhqtnRrM05zQpDSPhSvHB1nv+qMw5pvJEK/YGDxm3zeEzef/vQhti8IFSo86cF9WMxWxw==";
        String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDMCX+kg7jYq0Cpev6YAt1kA1ddJj9jMMIr6s3G9AR+IxPMCHmjhkOXgcPJ3Ls9cQBSwOkRNFOyP0cQXoSBgikjGBWH79TM3dMt1CF6pCVAsjYKyU7t75RL8nRtz1TnlZ9I2+heeUyY9XDvxLNeJ8FYbJIz5Pxbe37a6qBAZoKInQIDAQAB";
        //String privateKey=    "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAILFLdmFwfNHCGk3vpwj/FR9XpE9jjVreLlOaIEkSAOWOfzxCF7pMeSy0UDroRLgfLD4nQbUcK5AT7gg8PEM6H99SEHcpBkTKhfK1iI5iU9sN/WGL2Ft5T/uwo+KD7X3CsqMD0HsMQtv/YHJeAz/SPf5zyD5KttwohXXzO8a3P1pAgMBAAECgYAchxl2f6iNAu0Bzyhk9bDBWcw8kRop6zUd7836hkizh51E4ew6kFLTGnNt3zl3XcO187aF2+htCxiZCY6md3NstJod1zshoXf2slxxPWRUK92sS0XzlVT6ahVTdq2tAS3hOY3ldtnfOXMmMQPNn9OqlALQGOH/hJatBV6ZQxpp0QJBAMGKsxJkg3M6sZEO4zUgWgnGN+uS6nwAWpCfBY7eDvYSN78BxGGLgn+t40c0R8M/n85IM8OUmfHxTGWN3hYyGuUCQQCs+Kg1ypul9XLTWrVApyvZxY4ZxjAWG4yW4xqbUtHWUkGyle7Dy8xiYezl0kWN81lJNTz3qrJ4ZvM825+7tPw1AkBDPNxZV3ITZiCqNHHa0xJ0sthajv/HdJgCBjz4FU09T7buNL705HLByLdc1VzZCBGMqKjTGZ0h4KKZ5V9ydpXBAkABpZx0Zql0uxGM0aBILU9Nk4P22tw6WajNBvyJ3hABamcVvDe5xYb8qNIInifrYhXHjKo38XghjVljivPKZb7BAkAt4pCUYLI7LzLeSh/lnVxA7jOaRI3UsngCXn9VA4f+dkNcKAftRQqe7ytudqG07GKoInnDiyjrYe4kTLym8zoc";
            //String publicKey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCCxS3ZhcHzRwhpN76cI/xUfV6RPY41a3i5TmiBJEgDljn88Qhe6THkstFA66ES4Hyw+J0G1HCuQE+4IPDxDOh/fUhB3KQZEyoXytYiOYlPbDf1hi9hbeU/7sKPig+19wrKjA9B7DELb/2ByXgM/0j3+c8g+SrbcKIV18zvGtz9aQIDAQAB";
        String plain = "";//"plain：\"PLAIN\":{\"BODY\":{\"returnCode\":\"AAAAAAA\",\"status\":\"00\",\"returnMsg\":\"交易成功\",\"mobileNo\":\"15555524587\"},\"HEAD\":{\"spdbJnlNo\":\"997907074816\",\"timeStamp\":\"1435152316796\",\"transId\":\"MiguRepay\",\"jnlNo\":\"Y000Y021120140605\",\"version\":\"1.0\"}}";
        System.out.println(plain);
        //privateKey="MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAILFLdmFwfNHCGk3vpwj/FR9XpE9jjVreLlOaIEkSAOWOfzxCF7pMeSy0UDroRLgfLD4nQbUcK5AT7gg8PEM6H99SEHcpBkTKhfK1iI5iU9sN/WGL2Ft5T/uwo+KD7X3CsqMD0HsMQtv/YHJeAz/SPf5zyD5KttwohXXzO8a3P1pAgMBAAECgYAchxl2f6iNAu0Bzyhk9bDBWcw8kRop6zUd7836hkizh51E4ew6kFLTGnNt3zl3XcO187aF2+htCxiZCY6md3NstJod1zshoXf2slxxPWRUK92sS0XzlVT6ahVTdq2tAS3hOY3ldtnfOXMmMQPNn9OqlALQGOH/hJatBV6ZQxpp0QJBAMGKsxJkg3M6sZEO4zUgWgnGN+uS6nwAWpCfBY7eDvYSN78BxGGLgn+t40c0R8M/n85IM8OUmfHxTGWN3hYyGuUCQQCs+Kg1ypul9XLTWrVApyvZxY4ZxjAWG4yW4xqbUtHWUkGyle7Dy8xiYezl0kWN81lJNTz3qrJ4ZvM825+7tPw1AkBDPNxZV3ITZiCqNHHa0xJ0sthajv/HdJgCBjz4FU09T7buNL705HLByLdc1VzZCBGMqKjTGZ0h4KKZ5V9ydpXBAkABpZx0Zql0uxGM0aBILU9Nk4P22tw6WajNBvyJ3hABamcVvDe5xYb8qNIInifrYhXHjKo38XghjVljivPKZb7BAkAt4pCUYLI7LzLeSh/lnVxA7jOaRI3UsngCXn9VA4f+dkNcKAftRQqe7ytudqG07GKoInnDiyjrYe4kTLym8zoc";
        publicKey= "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCCxS3ZhcHzRwhpN76cI/xUfV6RPY41a3i5TmiBJEgDljn88Qhe6THkstFA66ES4Hyw+J0G1HCuQE+4IPDxDOh/fUhB3KQZEyoXytYiOYlPbDf1hi9hbeU/7sKPig+19wrKjA9B7DELb/2ByXgM/0j3+c8g+SrbcKIV18zvGtz9aQIDAQAB";

        SortedMap<String,String> map= new TreeMap<String,String>();
        /*map.put("openId","4325242342342343");
        map.put("receiveProduct","领取商品");
        map.put("status","1");
        map.put("timestamp","1597659579798");*/
        map.put("channelId","213");
        //map.put("data","LvYcN4HJCEEGxMoqfQ8hiS0TkmozZ24lKw0I%2FnPKb5iQ9gW9kk8LTgAsg5HQDhBfH4x9nEZ1vQhqiekLwphIbBnshpXrELHiCa%2BPb7sNG3Rur3dU8mHfr2dI81xHrj%2BP0aqQCoiyovuz7LT3TKOfcATZoFBwIddVoOWcVe%2FJct9a9VTs35T9bev2tqDxRxPgj3T0uG5GwkFHLvTjScKogNKRdb0yNtWczqA3s80B%2F9A%3D");
        //{"openId":"4325242342342343","receiveProduct":"领取商品","status":"1","timestamp":"1597659579798"}
        map.put("data","CsX8GJ1uAKJX6qO4a6%2BLPUyfC%2FJXv6zSQs7%2FPt1jp%2BRr2kpS8eOGZKi%2B0LvMtWk%2BjRTXmzm%2FsBD0jdpcTKbf0z7kxkY5ZEpeB57puWDPZEaz%2BCwDe18ryJzojoXvgNgU");

        Set<Map.Entry<String, String>> entries = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = entries.iterator();
        List<String> values = new ArrayList<>();

        while(iterator.hasNext()){
            Map.Entry entry = (Map.Entry) iterator.next();
            String k = String.valueOf(entry.getKey());
            String v = String.valueOf(entry.getValue());
            if (StringUtils.isNotEmpty(v) && entry.getValue() !=null && !"sign".equals(k) && !"key".equals(k)) {
                values.add(k + "=" + v);
            }
        }
        //values.add("key="+ key);
        //String sign = StringUtils.join(values, "&");
        String signStr = StringUtils.join(values, "&");
        System.out.println("签名Str："+signStr);
        //signStr="channelId=17&data=moEocxuFvUxHLhtWnI0e%2B0K1UDJfJ9BYEvXwXdbzeoasOOVtJ484%2B8SXdS7gGAcjlMmZL7vcCdw1OI3Fz2SfCwkrC9grlhWHzEtyq%2FRgzcGl%2B%2B5LjstFJidYtb98qGXo1xwYb%2B4Lrw9%2FwOua36eUBlr2KwV2s2jfz0PPPKfDRj3HpV%2BLuta%2Ffm4GsVz8PmKA";


        System.out.println(JSON.toJSONString(map));
        String sign = sign(signStr, privateKey);
        sign="O4Pr9BhBOkS7TDWCvyxAsyMmQavasaLjb3h2puTZUElAchYqxMeLfE0uaE8efVCJdD5XycGl9N5B/QKYHQYb4suQvFkpNZ0kBWijmXRHmIZD6bJo15rWFZnYse5TPCSiH9+TbiTccvv1TDp+jPWePeDfILT3OFK0BLhPt2cDg3g=";
        //sign="bP0M4V7hPuXR9EUX2icI7jo0K40uvZWGyCncN8e91xLks6g0M7wY0qv9mZaD9xT5NODGHFEywDhRLVZlG4Wzr2oinw2S/xxboKDgTfjpHo25RXWTtZ0eiK0hhyPPKIY6v7bYkFrupFqbq/PVjvdykyOAVQJxk9dHUuyjYpsMVBw=";
        //String sign="Ul8kHrAT/NzQc9MxAzQCZN8MsuK6+S58JejH0z9QJISHax0EKbaYCg+8CZlyI107SHYiiIUrwtlErcXySndWuzzOCoqrkSqG9O1Pts0fZ2rTTcvZi0EP6LLmUtaPyDq3fXkJwvkPBcW6DgKqyTZv7nsmXy3juMshnjGXOwNyuxg=";
        //签名信息
        System.out.println("签名="+sign);

        //签名信息
        //String sign = "BYyaHBgXhAZcjW0VUW1Cx7IpACMCkdmLkF5WkkgVEJboNtDzbQ0hRJ6v6xYDCrHKwTTigq9VpVKnyWAdvYkXlQyTs5vK0wx9aPlLaPFj6e8PZfd3+GM+Azwt15vgoaLs6GxcAZJ7FQMVkRqZWRv1MNorMh0rPLNwbdZgVF3m1+g=";

        //String sign1="ZEc0a25VSFhYalNtcEV3Tk5mOVZ0aElOTHJVRTh1cmg5S0lLM3JEZ1pHZnBkY3FuZVRaemhiM0pDa0s5emhZQzc3WWZ4N3ZQWURzZ1htV1BzNUtEQjltcFRCWldiSlI2RE12dW9LNGhYbUtFS2ROQVdTdnNnWk5Ca1FTWDJId3k4d3ZLQk1pZ0U5OFRQZVRobGF2TUEvS2FuTTQ0K05";
        //MGalgz+9miJ+CnZrdZu7mU8l0Q3N7inLm6EXl9PfNdREvaTtC2f6Jnvm75JG50Ss56bTcpGA6qEdV62N+lYZVHLqrn3LKRzCkDkOKgE2nMvAWuqStzx5P3g5L7ftbx4d0cH7j7tN4/E3WXr4fgwEtrkkLdbYucppOGKRcadRKsE=
        //验签结果（接收方使用对方公钥验签）
        //sign="NDp+DdnM6eFocKyOBGLHi2EzlGrzGYnGfNc35cOXZJnzThfcyV1Ngdk95pZfGSuQlKqnX8RzbJEzQN3I8CeWB/BlGHodtUDaegQDLC9gHDN/DRx8IMrEzRyy1gpz8JatDLiAviYDjKJwb0Smfkuz3YeQ2VqgysWi9jBnCb2BU50=";
        sign="Ul8kHrAT/NzQc9MxAzQCZN8MsuK6+S58JejH0z9QJISHax0EKbaYCg+8CZlyI107SHYiiIUrwtlErcXySndWuzzOCoqrkSqG9O1Pts0fZ2rTTcvZi0EP6LLmUtaPyDq3fXkJwvkPBcW6DgKqyTZv7nsmXy3juMshnjGXOwNyuxg=";
        System.out.println(virefy(signStr, sign, publicKey));

        //组装报文BODY内的内容
        Map<String, String> dataMap = new TreeMap();
        dataMap.put("param1", "value1");
        dataMap.put("param2", "value2");
        dataMap.put("param3", "value3");
        //生成最终报文（Json）
        //String body = JsonHelper.preparePostData("1", "1", "subOrgId", dataMap);
        //System.out.println(body);
    }

}