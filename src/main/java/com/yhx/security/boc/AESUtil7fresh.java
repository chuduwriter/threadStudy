package com.yhx.security.boc;

/**
 * @Title: AESUtil.java
 * @Package com.guanhuaWang.util.AESUtil
 * @Description: AES密码工具类
 * @date 2019年1月15日16:22:57
 * @version V1.0
 */

import com.yhx.myDemo.Student;
import com.yhx.testDemo.FastJsonUtils;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.Security;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: AESUtil
 * @date 2019年1月15日16:00:39
 */
public class AESUtil7fresh {

    /***默认向量常量**/
    public static final String IV = "878zxc321smn7d9s";
    private final static Logger LOGGER = LoggerFactory.getLogger(AESUtil7fresh.class);

    /**
     * 使用PKCS7Padding填充必须添加一个支持PKCS7Padding的Provider
     * 类加载的时候就判断是否已经有支持256位的Provider,如果没有则添加进去
     */
    static {

            Security.addProvider(new BouncyCastleProvider());

    }



    /**
     * 加密 128位
     *
     * @param content 需要加密的原内容
     * @param pkey    密匙
     * @return
     */
    public static byte[] aesEncrypt(String content, String pkey) {
        try {
            SecretKeySpec skey = new SecretKeySpec(pkey.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");// "算法/加密/填充"
            IvParameterSpec iv = new IvParameterSpec(IV.getBytes());
            cipher.init(Cipher.ENCRYPT_MODE, skey, iv);//初始化加密器
            byte[] encrypted = cipher.doFinal(content.getBytes("UTF-8"));
            return encrypted; // 加密
        } catch (Exception e) {
            LOGGER.info("aesEncrypt() method error:", e);
        }
        return null;
    }



    /**
     * @param content 加密前原内容
     * @param pkey    长度为16个字符,128位
     * @return base64EncodeStr   aes加密完成后内容
     * @throws
     * @Title: aesEncryptStr
     * @Description: aes对称加密
     */
    public static String aesEncryptStr(String content, String pkey) throws UnsupportedEncodingException {

        content=Base64.encodeBase64String(content.getBytes("UTF-8"));
        byte[] aesEncrypt = aesEncrypt(content, pkey);
        LOGGER.info("加密后的byte数组:" + Arrays.toString(aesEncrypt));
        String base64EncodeStr = Base64.encodeBase64String(aesEncrypt);
        LOGGER.info("加密后 base64EncodeStr:" + base64EncodeStr);
        base64EncodeStr= URLEncoder.encode(base64EncodeStr,"UTF-8");
        return base64EncodeStr;
    }

    /**
     * @param content base64处理过的字符串
     * @param pkey    密匙
     * @return String    返回类型
     * @throws Exception
     * @throws
     * @Title: aesDecodeStr
     * @Description: 解密 失败将返回NULL
     */
    public static String aesDecodeStr(String content, String pkey) throws Exception {
        try {
            content=URLDecoder.decode(content,"UTF-8");
            LOGGER.info("待解密内容:" + content);
            byte[] base64DecodeStr = Base64.decodeBase64(content);
            LOGGER.info("base64DecodeStr:" + Arrays.toString(base64DecodeStr));
            byte[] aesDecode = aesDecode(base64DecodeStr, pkey);
            LOGGER.info("aesDecode:" + Arrays.toString(aesDecode));
            if (aesDecode == null) {
                return null;
            }
            String result;
            result = new String(Base64.decodeBase64(aesDecode), "UTF-8");
            LOGGER.info("aesDecode result:" + result);
            return result;
        } catch (Exception e) {
            LOGGER.info("Exception:" + e.getMessage());
            throw new Exception("解密异常");
        }
    }

    /**
     * 解密 128位
     *
     * @param content 解密前的byte数组
     * @param pkey    密匙
     * @return result  解密后的byte数组
     * @throws Exception
     */
    public static byte[] aesDecode(byte[] content, String pkey) throws Exception {
        SecretKeySpec skey = new SecretKeySpec(pkey.getBytes(), "AES");
        IvParameterSpec iv = new IvParameterSpec(IV.getBytes("UTF-8"));
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");// 创建密码器
        cipher.init(Cipher.DECRYPT_MODE, skey, iv);// 初始化解密器
        byte[] result = cipher.doFinal(content);
        return result; // 解密

    }

    public static void main(String[] args) throws Exception {
        //密匙
        String pkey = "2j8s2jjugyuhgdnf";
        String aesEncryptStr=aesEncryptStr("zhangsan",pkey);
        System.out.println(pkey);
        String aesDecodeStr = aesDecodeStr(aesEncryptStr, pkey);

        Map<String,String> map=new HashMap<>();
        map.put("zhangan",null);
        map.put("lisi","lisi");
        System.out.println(FastJsonUtils.toJSONString(map));
        Student student=new Student();
        student.setName(null);
        System.out.println(FastJsonUtils.toJSONString(student));
    }

}


