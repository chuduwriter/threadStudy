package com.yhx.security.JdGuomi;

import java.math.BigInteger;

public class ROX {

    public static void main(String[] args) {
        String a="468A72F8318B43BC2A9E9E281A20C881";
        String b="EE22D57A302F7B0209E544A00FC1FC42";
        String c="2FE686AE6AD80D76BDAEFB9CB5EAE088";

       // String d=xor(xor(a,b),c);
        String d=hexXOR(hexXOR(a,b),c);
        System.out.println(d);


    }





    public static String xorHex(String a, String b) {
        char[] chars = new char[a.length()];
        for (int i = 0; i < chars.length; i++) {
            chars[i] = toHex(fromHex(a.charAt(i)) ^ fromHex(b.charAt(i)));
        }
        return new String(chars);
    }

    private static int fromHex(char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        if (c >= 'A' && c <= 'F') {
            return c - 'A' + 10;
        }
        if (c >= 'a' && c <= 'f') {
            return c - 'a' + 10;
        }
        throw new IllegalArgumentException();
    }

    private static char toHex(int nybble) {
        if (nybble < 0 || nybble > 15) {
            throw new IllegalArgumentException();
        }
        return "0123456789ABCDEF".charAt(nybble);
    }


    private static String hexXOR(String hex1, String hex2){
        BigInteger i1 = new BigInteger(hex1, 16);
        BigInteger i2 = new BigInteger(hex2, 16);
        BigInteger res = i1.xor(i2);
        return res.toString(16).toUpperCase();
    }







    private static String xor(String strHex_X,String strHex_Y){
        //将x、y转成二进制形式


        //String anotherBinary=Long.toBinaryString(Long.parseLong(strHex_X,16));

        byte [] bArray = HexStringToBinary(strHex_X);
        String anotherBinary= new String(bArray);

        byte [] sArray = HexStringToBinary(strHex_Y);
        String thisBinary= new String(sArray);
        //String thisBinary=Long.toBinaryString(Long.parseLong(strHex_Y,16));


        System.out.println("将str的十六进制文件转换为二进制再转为String：\n"+new String(bArray));
        String result = "";
        //判断是否为8位二进制，否则左补零
        if(anotherBinary.length() != 8){
            for (int i = anotherBinary.length(); i <8; i++) {
                anotherBinary = "0"+anotherBinary;
            }
        }
        if(thisBinary.length() != 8){
            for (int i = thisBinary.length(); i <8; i++) {
                thisBinary = "0"+thisBinary;
            }
        }
        //异或运算
        for(int i=0;i<anotherBinary.length();i++){
            //如果相同位置数相同，则补0，否则补1
            if(thisBinary.charAt(i)==anotherBinary.charAt(i))
                result+="0";
            else{
                result+="1";
            }
        }
        return Integer.toHexString(Integer.parseInt(result, 2));
    }



    private static String hexStr =  "0123456789ABCDEF";
    private static String[] binaryArray =
            {"0000","0001","0010","0011",
                    "0100","0101","0110","0111",
                    "1000","1001","1010","1011",
                    "1100","1101","1110","1111"};

 /*   public static void main(String[] args)
    {
        String str = "二进制与十六进制互转测试";
        System.out.println("源字符串：\n"+str);

        String hexString = BinaryToHexString(str.getBytes());
        System.out.println("转换为十六进制：\n"+hexString);
        System.out.println("转换为二进制：\n"+bytes2BinaryStr(str.getBytes()));

        byte [] bArray = HexStringToBinary(hexString);
        System.out.println("将str的十六进制文件转换为二进制再转为String：\n"+new String(bArray));

    }*/
    /**
     *
     * @param str
     * @return 转换为二进制字符串
     */
    public static String bytes2BinaryStr(byte[] bArray){

        String outStr = "";
        int pos = 0;
        for(byte b:bArray){
            //高四位
            pos = (b&0xF0)>>4;
            outStr+=binaryArray[pos];
            //低四位
            pos=b&0x0F;
            outStr+=binaryArray[pos];
        }
        return outStr;

    }
    /**
     *
     * @param bytes
     * @return 将二进制转换为十六进制字符输出
     */
    public static String BinaryToHexString(byte[] bytes){

        String result = "";
        String hex = "";
        for(int i=0;i<bytes.length;i++){
            //字节高4位
            hex = String.valueOf(hexStr.charAt((bytes[i]&0xF0)>>4));
            //字节低4位
            hex += String.valueOf(hexStr.charAt(bytes[i]&0x0F));
            result +=hex+" ";
        }
        return result;
    }
    /**
     *
     * @param hexString
     * @return 将十六进制转换为字节数组
     */
    public static byte[] HexStringToBinary(String hexString){
        //hexString的长度对2取整，作为bytes的长度
        int len = hexString.length()/2;
        byte[] bytes = new byte[len];
        byte high = 0;//字节高四位
        byte low = 0;//字节低四位

        for(int i=0;i<len;i++){
            //右移四位得到高位
            high = (byte)((hexStr.indexOf(hexString.charAt(2*i)))<<4);
            low = (byte)hexStr.indexOf(hexString.charAt(2*i+1));
            bytes[i] = (byte) (high|low);//高地位做或运算
        }
        return bytes;
    }

}
