/**
 * 
 */
package com.yhx.security.demo.parameter;

/**
 * @version 1.0
 * @Author zhangxiaoli5
 * @Description 申请信息入参
 * @Date 2019/8/9 16:52
 **/
public class ApplyRequest
{
	private String requestNo;
	
	private String mobile;
	
	private String type;
	
	private String biz;

	public ApplyRequest() {
	}

	public ApplyRequest(String requestNo, String mobile, String type, String biz) {
		this.requestNo = requestNo;
		this.mobile = mobile;
		this.type = type;
		this.biz = biz;
	}

	public String getRequestNo() {
		return requestNo;
	}

	public void setRequestNo(String requestNo) {
		this.requestNo = requestNo;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBiz() {
		return biz;
	}

	public void setBiz(String biz) {
		this.biz = biz;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("ApplyRequest{");
		sb.append("requestNo='").append(requestNo).append('\'');
		sb.append(", mobile='").append(mobile).append('\'');
		sb.append(", type='").append(type).append('\'');
		sb.append(", biz='").append(biz).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
