package com.yhx.security.guangfa;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class VerfyAndSignOut{

	public static final String  LOCALPATHZIP = "src/main/webapp/cert/20201130";
	/**
	 *
	 * @param CGBPack 压缩包
	 * @param outputFile 最终获取到的文件
	 * @param certPath 密钥文件
	 * @return
	 * @throws Exception
	 */
	public static boolean excute(String CGBPack,String outputFile,String certPath) throws Exception {
		String GFPukPath = "src/main/webapp/cert/cert/100000/100000.puk";
		String signfile = LOCALPATHZIP +"/signature.txt";
		String desfile = LOCALPATHZIP + "/encryptKey.txt";
		String srcfile ="src/main/webapp/cert/20201130/CGBPack";//  CGBPack.substring(CGBPack.lastIndexOf("/")+1, CGBPack.lastIndexOf("."));
		String unzipfilePath = LOCALPATHZIP;
		createFile(unzipfilePath);
		//解出文件
		ZipFileUnpack.unpacker(CGBPack, unzipfilePath);
		boolean result = descrypt(signfile, desfile, srcfile, certPath, outputFile, GFPukPath);

		cleanFile(signfile);
		cleanFile(desfile);
		cleanFile(srcfile);
		delFile("src/main/webapp/cert/20201206");
		return result;
	}

	public static void main(String[] args) {
		delFile("src/main/webapp/cert/20201206/CGBPack.zip");
	}
	/**
	 * 根据路径删除指定的目录或文件，无论存在与否
	 *
	 * @param sPath
	 *            要删除的目录或文件
	 * @return 删除成功返回 true，否则返回 false。
	 */
	static boolean delFile(String filename) {
		File file = new File(filename);
		if (!file.exists()) {
			return false;
		}

		if (file.isFile()) {
			return file.delete();
		} else {
			String[] filenames = file.list();
			for (String f : filenames) {
				delFile(f);
			}
			return file.delete();
		}
	}

	/**
	 * 解出文件
	 * @param flie
	 * @param br
	 * @param ins
	 * @return
	 * @throws IOException
	 */
	public static String sreamFile(File flie) throws IOException {
		String strValue = null;
		StringBuffer resBuff = new StringBuffer();;
		FileInputStream fis = null;
		InputStreamReader ins = null;
		BufferedReader br = null;
		try {
			fis = new FileInputStream(flie);
			ins = new InputStreamReader(fis);
			br = new BufferedReader(ins);
			while((strValue = br.readLine())!=null) {
				resBuff.append(strValue);
			}
		} catch (IOException e) {
		}finally {
			if(br != null) {
				br.close();
			}
			if(ins != null) {
				ins.close();
			}
			if(fis != null) {
				fis.close();
			}
		}

		return resBuff.toString();

	}

	/**
	 *
	 * @param signfile
	 * @param desfile
	 * @param srcfile
	 * @throws Exception
	 */
	public static boolean descrypt(String signfile,String desfile,String srcfile,String certPath,String outfilePath,String pukey) throws Exception {
		File sigf = new File(signfile);
		File desf = new File(desfile);
		boolean fila = false;
		try {

			String desStr = sreamFile(desf);
			String signStr = sreamFile(sigf);

			//解出随机密钥
			byte[] desByte =  SM2SignUtil.decryptString(SM2SignUtil.buildPrivateKey(certPath), desStr);
			//解密文件
			boolean result=  SM2SignUtil.SM4DecryptFile(new String(desByte), srcfile, outfilePath);

			//验证签名
			if(result) {
				fila = SM2SignUtil.verifilyFile(outfilePath, pukey, signStr);
			}

		} catch (Exception e) {
			throw new Exception( "文件解密失败", e);
		}

		return fila;

	}

	/**
	 *
	 * @param filePath
	 * @throws RtpTranException
	 */
	private static void createFile(String filePath) throws Exception {
		File pathFile = new File(filePath);
		if(!pathFile.exists()) {
			pathFile.mkdirs();
		}
		if(!pathFile.exists()) {
			throw new Exception("无法创建文件路径");
		}
	}


	public static void cleanFile(String filePath) {
		File fileToClean = new File(filePath);
		if(fileToClean.exists()) {
			fileToClean.delete();
		}
	}


	public static String formatDateTimeyyyyMM(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		return dateFormat.format(date);
	}
}
