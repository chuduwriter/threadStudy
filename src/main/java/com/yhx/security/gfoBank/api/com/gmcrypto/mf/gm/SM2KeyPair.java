package com.yhx.security.gfoBank.api.com.gmcrypto.mf.gm;

import java.util.Base64;

/**
 * sm2密钥对实体类
 *
 */
public class SM2KeyPair {
	
	private String base64PriKey;
	private String  base64PubKey;
	private byte[] priByte;
	private byte[] pubByte;
	
	public SM2KeyPair(byte[] priByte,byte[] pubByte) {
		this.priByte=priByte;
		this.pubByte=pubByte;
	}

	public String getBase64PriKey() {
		if(base64PriKey==null) {
			base64PriKey=Base64.getEncoder().encodeToString(priByte);
		}
		return base64PriKey;
	}

	public String getBase64PubKey() {
		if(base64PubKey==null) {
			base64PubKey=Base64.getEncoder().encodeToString(pubByte);
		}
		return base64PubKey;
	}


	public byte[] getPriByte() {
		return priByte;
	}

	public void setPriByte(byte[] priByte) {
		this.priByte = priByte;
	}

	public byte[] getPubByte() {
		return pubByte;
	}

	public void setPubByte(byte[] pubByte) {
		this.pubByte = pubByte;
	}
}
