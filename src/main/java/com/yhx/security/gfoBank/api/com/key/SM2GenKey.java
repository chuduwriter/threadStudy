package com.yhx.security.gfoBank.api.com.key;

import java.io.File;


import com.yhx.security.gfoBank.api.com.gmcrypto.mf.gm.SM2KeyPair;
import com.yhx.security.gfoBank.api.com.gmcrypto.mf.gm.SM2Util;

public class SM2GenKey {

	public static void main(String[] args) throws Exception {
		String keyName="jdTest";
		KeyGen(keyName);
	}


	private static void KeyGen(String merName) throws Exception {
		SM2KeyPair keyPair= SM2Util.generateKeyPair();

		File dir = new File("cert/" + merName);
		if (!dir.exists()) {
			System.out.println("是否生成目录：" + dir.mkdirs());
		}

		FileHelper.write("cert/" + merName + "/" + merName + ".pvk",
				keyPair.getPriByte());
		FileHelper.write("cert/" + merName + "/" + merName + ".puk",
				keyPair.getPubByte());
				
		System.out.println("密钥生成成功：" + merName);
	}
}
