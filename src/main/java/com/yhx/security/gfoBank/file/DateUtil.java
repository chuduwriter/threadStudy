package com.yhx.security.gfoBank.file;

import java.util.Calendar;

/**
 * @author: mishuai
 * @date: 2020/12/3 17:05
 * @description:
 */
public class DateUtil {

    public static void main(String[] args) {
        System.out.println(getCurrentDayTimer(8));
    }


    public static long getCurrentDayTimer(Integer hour){
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DATE);
        c.set(year, month, day, hour, 0, 0);
        return c.getTimeInMillis();
    }
}
