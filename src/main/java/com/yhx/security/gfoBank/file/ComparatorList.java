package com.yhx.security.gfoBank.file;

import com.yhx.testDemo.ShopAddressInfo;

/**
 * @author: mishuai
 * @date: 2020/12/4 19:03
 * @description:
 */
public class ComparatorList implements java.util.Comparator {

        /**
         * 根据生成时间进行排序
         * @param arg0
         * @param arg1
         * @return
         */
        @Override
        public int compare(Object arg0, Object arg1) {
            ShopAddressInfo shop0=(ShopAddressInfo)arg0;
            ShopAddressInfo shop1=(ShopAddressInfo)arg1;
            return shop0.getFrontTow().compareTo(shop1.getFrontTow());
        }


}
