/*
package com.yhx.security.icbcGuomi;

import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;

import java.io.IOException;
import java.math.BigInteger;

*/
/**
 *
 * @ClassName: DemoMain
 * @Description: TODO(国密SM2签名验签/SM3报文摘要)
 * @date 2019年5月10日
 *//*


public class DemoMain {
	// 国密规范测试用户ID
	private static final String userId = "1234567812345678";
	// 国密规范测试私钥
	//private static final String prik = "211E7686A0BCAFF554F0FD366F428E40A2C4FC1AB6E7E7F74E8696494B723AA2";
	//private static final String prik ="63C8A82ED4FCD5E234E3DB6651327585F33C2149D36FC792345110FD50328B4A";
	private static final String prik="3945208f7b2144b13f36e38ac6d39f95889393692860b51a42fb81ef4df7c5b8";
	//国密规范测试公钥
	//private static final String pubk = "0402B1B28FB8C84696417DD172FBA233DEB751D871772BDE2A48E7964CDFE468DC02197EB57F20FDF341860B9A2006E76377AC2507D454BC217D3F22D611284D15";
	//private static final String pubk ="041844605D9615DE908723507A7AA394F3CBEEDAA97028D8BCA82EC8AC43328054573140E7D1B9F886D906FF23D9F8475E2E2BAEB1846B2DB9CC518B1F71D4B9E4";

	//private static final String pubk ="04"+"09f9df311e5421a150dd7d161e4bc5c672179fad1833fc076bb08ff356f35020"+"ccea490ce26775a52dc6ea718cc1aa600aed05fbf35e084a6632f6072da9ad13";
	String signStr="MEQCICByA4m8rDjer6lI1J09Y3f9gKpAY21eisopw8iYLXQjAiAkH3/B9IOfZPw5u20cjH/y9VMQnMPH6jk5KyV0vXdKhw==";
	// 国密规范测试公钥
	private static final String pubk = "04A9E343C21B3DE3062C79822CD122222A1BBCC6DA0A3B527AEAD56772EC6AEC1B17A50DD7D95B74CF007C07DE8562F50521A8D308603F239AA8CF8F2620660163";

	public static void main(String[] arg) {
		//createKey();
		String msg = "123456789";//原始数据
		System.out.println("原始数据："+msg);
		String summaryString = summary(msg);
		System.out.println("摘要："+summaryString);
		String signString = sign(summaryString);
		System.out.println("摘要签名："+signString);
		boolean status = verify(summaryString,signString);
		System.out.println("验签结果："+status);

		System.out.println("加密: ");
		byte[] cipherText = null;
		try {
			cipherText = SM2Utils.encrypt(Base64.decode(new String(Base64.encode(Util.hexToByte(pubk))).getBytes()), msg.getBytes());
		} catch (IllegalArgumentException e1) {
			// TODO 自动生成的 catch 块
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO 自动生成的 catch 块
			e1.printStackTrace();
		}
		System.out.println(new String(Base64.encode(cipherText)));
		System.out.println("");

		System.out.println("解密: ");
		String res = null;
		try {
			res = new String(SM2Utils.decrypt(Base64.decode(new String(Base64.encode(Util.hexToByte(prik))).getBytes()), cipherText));
		} catch (IllegalArgumentException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		System.out.println(res);

	}

	*/
/**
	 * 摘要
	 * @return *//*



	public static String summary(String msg) {
		//1.摘要
		byte[] md = new byte[32];
		SM3Digest sm = new SM3Digest();
		sm.update(msg.getBytes(), 0, msg.getBytes().length);
		sm.doFinal(md, 0);
		String s = new String(Hex.encode(md));
		return s.toUpperCase();
	}

	*/
/**
	 * 签名
	 * @return
	 *//*


	public static String sign(String summaryString) {
		String prikS = new String(Base64.encode(Util.hexToByte(prik)));
		System.out.println("prikS: " + prikS);
		System.out.println("");

		System.out.println("ID: " + Util.getHexString(userId.getBytes()));
		System.out.println("");
		System.out.println("签名: ");
		byte[] sign = null; //摘要签名
		try {
			sign = SM2Utils.sign(userId.getBytes(), Base64.decode(prikS.getBytes()), Util.hexToByte(summaryString));
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Util.getHexString(sign);
	}

	*/
/**
	 * 验签
	 * @return *//*



	public static boolean verify(String summary,String sign) {
		String pubkS = new String(Base64.encode(Util.hexToByte(pubk)));
		System.out.println("pubkS: " + pubkS);
		System.out.println("");

		System.out.println("验签 ");
		boolean vs = false; //验签结果
		try {
			vs = SM2Utils.verifySign(userId.getBytes(), Base64.decode(pubkS.getBytes()), Util.hexToByte(summary), Util.hexToByte(sign));
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return vs;
	}

	*/
/**
	 * 生成随机密钥对*//*



	public static void createKey() {
		SM2 sm2 = SM2.Instance();
		AsymmetricCipherKeyPair key = sm2.ecc_key_pair_generator.generateKeyPair();
		ECPrivateKeyParameters ecpriv = (ECPrivateKeyParameters) key.getPrivate();
		ECPublicKeyParameters ecpub = (ECPublicKeyParameters) key.getPublic();
		BigInteger privateKey = ecpriv.getD();
		ECPoint publicKey = ecpub.getQ();

		System.out.println("公钥: " + Util.byteToHex(publicKey.getEncoded()));
		System.out.println("私钥: " + Util.byteToHex(privateKey.toByteArray()));
	}

}
*/
