package com.yhx.security.icbcGuomi;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SM4Utils {
	private String secretKey = "";
	
	private String iv = "";
	
	private boolean hexString = true;
	
	public SM4Utils() {
	}
	
	public String encryptData_ECB(String plainText) {
		try {
			SM4_Context ctx = new SM4_Context();
			ctx.isPadding = true;
			ctx.mode = SM4.SM4_ENCRYPT;

			byte[] keyBytes;
			if (hexString) {
				keyBytes = Util.hexStringToBytes(secretKey);
			} else {
				keyBytes = secretKey.getBytes();
			}

			SM4 sm4 = new SM4();
			sm4.sm4_setkey_enc(ctx, keyBytes);
			byte[] encrypted = sm4.sm4_crypt_ecb(ctx, plainText.getBytes("UTF-8"));
			String cipherText = new BASE64Encoder().encode(encrypted);
			if (cipherText != null && cipherText.trim().length() > 0) {
				Pattern p = Pattern.compile("\\s*|\t|\r|\n");
				Matcher m = p.matcher(cipherText);
				cipherText = m.replaceAll("");
			}
			return cipherText;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String decryptData_ECB(String cipherText) {
		try {
			SM4_Context ctx = new SM4_Context();
			ctx.isPadding = true;
			ctx.mode = SM4.SM4_DECRYPT;

			byte[] keyBytes;
			if (hexString) {
				keyBytes = Util.hexStringToBytes(secretKey);
			} else {
				keyBytes = secretKey.getBytes();
			}

			SM4 sm4 = new SM4();
			sm4.sm4_setkey_dec(ctx, keyBytes);
			byte[] decrypted = sm4.sm4_crypt_ecb(ctx, new BASE64Decoder().decodeBuffer(cipherText));
			return new String(decrypted, "UTF-8");
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String encryptData_CBC(String plainText) {
		try {
			SM4_Context ctx = new SM4_Context();
			ctx.isPadding = true;
			ctx.mode = SM4.SM4_ENCRYPT;

			byte[] keyBytes;
			byte[] ivBytes;
			if (hexString) {
				keyBytes = Util.hexStringToBytes(secretKey);
				ivBytes = Util.hexStringToBytes(iv);
			}else{
				keyBytes = secretKey.getBytes();
				ivBytes = iv.getBytes();
			}

			SM4 sm4 = new SM4();
			sm4.sm4_setkey_enc(ctx, keyBytes);
			byte[] encrypted = sm4.sm4_crypt_cbc(ctx, ivBytes, plainText.getBytes("UTF-8"));
			String cipherText = new BASE64Encoder().encode(encrypted);
			if (cipherText != null && cipherText.trim().length() > 0) {
				Pattern p = Pattern.compile("\\s*|\t|\r|\n");
				Matcher m = p.matcher(cipherText);
				cipherText = m.replaceAll("");
			}
			return cipherText;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String decryptData_CBC(String cipherText) {
		try {
			SM4_Context ctx = new SM4_Context();
			ctx.isPadding = true;
			ctx.mode = SM4.SM4_DECRYPT;
			
			byte[] keyBytes;
			byte[] ivBytes;
			if (hexString) {
				keyBytes = Util.hexStringToBytes(secretKey);
				ivBytes = Util.hexStringToBytes(iv);
			} else {
				keyBytes = secretKey.getBytes();
				ivBytes = iv.getBytes();
			}
			
			SM4 sm4 = new SM4();
			sm4.sm4_setkey_dec(ctx, keyBytes);
			byte[] decrypted = sm4.sm4_crypt_cbc(ctx, ivBytes, new BASE64Decoder().decodeBuffer(cipherText));
			return new String(decrypted, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String[] args) throws IOException  {
		String plainText = "ab张cd";

		SM4Utils sm4 = new SM4Utils();

	/*	String str=HexUtils.hexString2String("77EA0895C83359B93E19100F94F890B4","UTF-8","UTF-8");

		byte[] bts=HexUtils.hexString2Bytes("77EA0895C83359B93E19100F94F890B4");
	*/	//sm4.secretKey ="77EA0895C83359B93E19100F94F890B4";
	    sm4.secretKey="5DE05FA19087ADB3B506D6DD9CD87F17";
		sm4.hexString = true;
		
/**/

		System.out.println("ECB模式");
		String cipherText = sm4.encryptData_ECB(plainText);
		System.out.println("密文: " + cipherText);
		System.out.println("");
        cipherText="NjY5ZGQ3YTI4MDZmODE2NTliMjZiOTUxMjRiZTBjNzAxM2IzZjgyZDExNTM4NmQ0YzU2N2ZiZDJjNDliY2Q1Mw==";
		plainText = sm4.decryptData_ECB(cipherText);
		System.out.println("明文: " + plainText);
		System.out.println("");



       //TvYuJcfazJlK/JIA05MvyQ==
		System.out.println("CBC模式");
		//sm4.iv = "33383339313735373735343938373931";
		sm4.iv="6344384F48314E7937756F614A734664";
		String cipherText1 = sm4.encryptData_CBC(plainText);
		System.out.println("密文: " + cipherText);
		System.out.println("");
		cipherText1="oGEWHrxftcRjjt2aLhqpJL5g3uiUqOC32D3jYs9bZ+KYpvTuEwWMmzpnTLgbn1VhOmieooO3AFdsYyX7nGVxA19sKSRpC1DzNCw7lQquEbrFV4ecNjBWwq4YJZUkWE3yOEm3Nt/7D1CUW9g1RjzqdTmmNRO6N8N2qdRWEcgRMGQ4GNABMkaJ9E4Kw/tkf4yo/eQHO16qBdRypz2kow3vrQ==";
		plainText = sm4.decryptData_CBC(cipherText1);
		System.out.println("明文: " + URLDecoder.decode(plainText,"UTF-8"));
	}
}
