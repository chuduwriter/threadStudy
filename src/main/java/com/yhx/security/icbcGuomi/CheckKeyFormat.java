/*
package com.yhx.security.icbcGuomi;

import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.util.encoders.Base64;
//bcmail-jdk15-1.46
//bcprov-jdk15.1.46

@SuppressWarnings("all")
public class CheckKeyFormat {

    */
/**
     * check sm2 PrivateKey Format
     *
     * @author 杜文磊
     *
     * *//*

    public static void main(String[] args) throws Exception {
//         String key = "-----BEGIN EC PRIVATE KEY-----"
//         + "MHcCAQEEIEzQsN3BumQJd5ri/9boIs8kluKKQNJ7DPxeT4TKSyPkoAoGCCqGSM49"
//         + "AwEHoUQDQgAEnr6UttffZuM/w8QehrCOtFm8FpjgJxHt+qRNcH0je+DSXVBJwZkV"
//         + "7slL7e1VbRe4mh0JsSuLu6XD5P3iHyOnsw=="
//         + "-----END EC PRIVATE KEY-----";
        String key = "-----BEGIN EC PRIVATE KEY-----"
                + "MHcCAQEEIGsDOEC1seWulpvO0N09WmKvCOhMAD0eJY5y1i+0Na6HoAoGCCqBHM9V"
                + "AYItoUQDQgAE450G+j/3ndC+BA30RmbCMjsh12DhGLOwXy8X2VNC8Zb6F9IDgakb"
                + "B47+n2N8ct8tryfMORh08QckBY66PtuUkQ=="
                + "-----END EC PRIVATE KEY-----";
        key = key.replace("-----BEGIN EC PRIVATE KEY-----", "");
        key = key.replace("-----END EC PRIVATE KEY-----", "");
        byte[] decKey = Base64.decode(key);
        DERSequence derSequence = (DERSequence) ASN1Sequence
                .fromByteArray(decKey);

        // 检查 version
        DEREncodable derIntegerValue = checkInteger_Version(derSequence);
        System.out.println("Integer : version = " + derIntegerValue);

        // 检查私钥长度
        DEREncodable derSM2PrivateValue = checkSM2Private_length(derSequence);
        System.out.println("SM2Private : length = "
                + ((DEROctetString) derSM2PrivateValue.getDERObject())
                .getOctets().length);

        // 检查OID
        DERObjectIdentifier derObjOID = checkOID(derSequence);
        System.out.println("OID = " + derObjOID);

        // 检查内容值
        DERBitString derBitEnd = checkContent(derSequence);
        System.out.println("Content = " + derBitEnd.getString());

        if(!derIntegerValue.toString().equals("1")){
            System.out.println(" version is not 1 ! ");
        }
        if(!derObjOID.toString().equals("1.2.156.10197.1.301")){
            System.err.println(" OID Not SM2 private key!");
//            System.exit(0);
        }

    }

    */
/************************************************************utils******************************************************************//*

    private static DERBitString checkContent(DERSequence derSequence) {
        DEREncodable derEnd = derSequence.getObjectAt(3);
        if (!(derEnd.getDERObject() instanceof DERTaggedObject)) {
            System.out.println(false);
        }

        DERTaggedObject derTagEnd = (DERTaggedObject) derEnd.getDERObject();
        if (!(derTagEnd.getObject() instanceof DERBitString)) {
            System.out.println(false);
        }
        DERBitString derBitEnd = (DERBitString) derTagEnd.getObject();
        return derBitEnd;
    }

    */
/**
     * @see 检查OID
     * @param derSequence
     * @return
     *//*

    private static DERObjectIdentifier checkOID(DERSequence derSequence) {
        DEREncodable derOID = derSequence.getObjectAt(2);
        if (!(derOID.getDERObject() instanceof DERTaggedObject)) {
            System.out.println(false);
        }
        DERTaggedObject derTagOID = (DERTaggedObject) derOID;
        if (!(derTagOID.getObject() instanceof DERObjectIdentifier)) {
            System.out.println(false);
        }
        DERObjectIdentifier derObjOID = (DERObjectIdentifier) derTagOID
                .getObject();
        return derObjOID;
    }

    */
/**
     * @see 检查私钥长度
     * @param derSequence
     * @return
     *//*

    private static DEREncodable checkSM2Private_length(DERSequence derSequence) {
        DEREncodable derSM2PrivateValue = derSequence.getObjectAt(1);
        if (!(derSM2PrivateValue.getDERObject() instanceof DEROctetString)) {
            System.out.println(false);
        }
        return derSM2PrivateValue;
    }

    */
/**
     * @see 检查私钥version
     * @param derSequence
     * @return
     *//*

    private static DEREncodable checkInteger_Version(DERSequence derSequence) {
        DEREncodable derIntegerValue = derSequence.getObjectAt(0);
        return derIntegerValue;
    }

}*/
